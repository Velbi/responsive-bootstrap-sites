#az account set -s $subscription # ...or use 'az login'
az login

# Création de la base de données
location="westus"
randomIdentifier="boostrapApp"

resource="$randomIdentifier-resource"
insight="$randomIdentifier-insight"

az extension add --name application-insights

echo -e "\E[32m Creating $resource \E[0m"
az group create --name $resource --location $location

echo -e "\E[32m Creating $resource \E[0m"
az monitor app-insights component create --app $insight --location $location --kind web -g $resource --application-type web --retention-time 120


$SHELL