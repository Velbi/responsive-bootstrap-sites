<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
<head>
	<title>Home</title>
</head>
<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
		<h1>
			Hello world!  
		</h1>

		<P>The time on the server is ${serverTime}.</P>
		<img class="featuredImg" src="resources/img/moto1.png" height="90%"/>
		</tiles:putAttribute>
	</tiles:insertDefinition>
</html>