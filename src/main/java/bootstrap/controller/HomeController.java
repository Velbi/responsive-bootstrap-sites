package bootstrap.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.ui.Model;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class HomeController {
	
	private static final Logger logger = LogManager.getLogger(HomeController.class);
	
	@RequestMapping(method = RequestMethod.GET)
	public String showPage(Locale locale, Model model) {
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		String formattedDate = dateFormat.format(date);
		model.addAttribute("serverTime", formattedDate );
		
		logger.info("<< Info - Welcome home! Time : "+formattedDate);
		logger.warn("<< WARN - Welcome home! Time : "+formattedDate);
		logger.debug("<< DEBUG - Welcome home! Time : "+formattedDate);
		
		return "home";
	}
}