package bootstrap.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/index")
public class IndexController {

    private static final Logger logger = LogManager.getLogger(IndexController.class);

    @RequestMapping(method = RequestMethod.GET)
    public String getIndexPage() {
        logger.info("Page index");
        return "index";
    }
}
